'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session')
const formidable = require('formidable');
const { request } = require('express');
const FacebookBot = require("./FacebookBot");
const axios = require('axios')

const app = express();
const DatabaseHandle = require("./Database");
const AwsDatabase = require("./AwsDatabase");
const { JSONParser } = require('formidable');
const BotClient = require('facebook-bot-messenger/lib/botmessenger/botclient');
const PAGEID = "hide";


app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: '863470',
    cookie: { maxAge: 900000 }
}));

const port = process.env.PORT || 4000;

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.listen(port, function() {
    console.log('Start web app Chat bot listening on port 4000!');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//#region MainFunction
//--------Route app--------------//
app.get('/', function(req, res) {
    res.send("Xin chào");
})

app.get('/webhook', function(req, res) {
    let VERIFY_TOKEN = 'hoanganhpro2';
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];
    console.log('Verify: ' + challenge);
    if (mode && token === VERIFY_TOKEN) {
        res.status(200).send(challenge);
    } else {
        res.sendStatus(403);
    }
})

app.post('/webhook', function(req, res) {
    res.status(200).send("OK");
    if (req.body.object === 'page') {
        var entries = req.body.entry;
        var facebookBot = new FacebookBot();
        facebookBot.GetMessage(entries);
    }
})

//#region Kich ban
app.post('/getlistscriptid', function(req, res) {
    (async() => {
        if (req.body.pageid != null) {
            var item = await DatabaseHandle.ReadListAllCommandActionID(req.body.pageid);
            res.send(CreateRespond(true, item));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

app.post('/getscript', function(req, res) {
    console.log(req.body);
    (async() => {
        if (req.body.pageid != null && req.body.itemid != null) {
            var item = await DatabaseHandle.ReadCommandAction(req.body.pageid, req.body.itemid);
            res.send(CreateRespond(true, item));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

app.post('/updatescript', function(req, res) {
    console.log(req.body);
    (async() => {
        if (req.body.pageid != null && req.body.itemid != null && req.body.item != null) {
            await DatabaseHandle.WriteCommandAction(req.body.pageid, req.body.itemid, req.body.item);
            res.send(CreateRespond(true, null));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

app.post('/deletescript', function(req, res) {
    console.log(req.body);
    (async() => {
        if (req.body.pageid != null && req.body.itemid != null) {
            await DatabaseHandle.DeleteCommandAction(req.body.pageid, req.body.itemid);
            res.send(CreateRespond(true, null));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

//#endregion

//#region San pham
app.post('/getlistproductid', function(req, res) {
    (async() => {
        if (req.body.pageid != null) {
            var item = await DatabaseHandle.GetListAllProductID(req.body.pageid);
            res.send(CreateRespond(true, item));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

app.post('/getproduct', function(req, res) {
    (async() => {
        if (req.body.pageid != null && req.body.itemid != null) {
            var item = await DatabaseHandle.GetProductDataWithID(req.body.pageid, req.body.itemid);
            res.send(CreateRespond(true, item));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

app.post('/deleteproduct', function(req, res) {
    (async() => {
        if (req.body.pageid != null && req.body.itemid != null) {
            var item = await DatabaseHandle.DeleteProductWithID(req.body.pageid, req.body.itemid);
            res.send(CreateRespond(true, item));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

app.post('/getuniqueproductid', function(req, res) {
    (async() => {
        if (req.body.pageid != null) {
            var item = await DatabaseHandle.GetUniqueProductID(req.body.pageid);
            res.send(CreateRespond(true, item));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})



app.post('/updateproduct', function(req, res) {
    console.log(req.body);
    (async() => {
        if (req.body.pageid != null && req.body.itemid != null && req.body.item != null) {
            await DatabaseHandle.UpdateProductDataWithID(req.body.pageid, req.body.itemid, req.body.item);
            res.send(CreateRespond(true, null));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

//#endregion

//#region Don hang

app.post('/getlistorderid', function(req, res) {
    (async() => {
        if (req.body.pageid != null) {
            var item = await DatabaseHandle.GetListUserOrderID(req.body.pageid);
            res.send(CreateRespond(true, item));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

app.post('/getorder', function(req, res) {
    (async() => {
        if (req.body.pageid != null && req.body.itemid != null) {
            var item = await DatabaseHandle.GetUserOder(req.body.pageid, req.body.itemid);
            res.send(CreateRespond(true, item));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

app.post('/updateorder', function(req, res) {
        console.log(req.body);
        (async() => {
            if (req.body.pageid != null && req.body.itemid != null && req.body.item != null) {
                await DatabaseHandle.WriteUserOrder(req.body.pageid, req.body.itemid, req.body.item);
                res.send(CreateRespond(true, null));
            } else {
                res.send(CreateRespond(false, null));
            }
        })();
    })
    //#endregion


app.post("/createnewuser", function(req, res) {
    (async() => {
        if (req.body.pageid != null) {
            await DatabaseHandle.CreateAllTableForUser(req.body.pageid);
            res.send(CreateRespond(true, null));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})

app.post("/updatetoken", function(req, res) {
    (async() => {
        if (req.body.pageid != null && req.body.item != null) {
            console.log(req.body.item);
            await DatabaseHandle.WriteUpdateToken(req.body.pageid, req.body.item);
            res.send(CreateRespond(true, null));
        } else {
            res.send(CreateRespond(false, null));
        }
    })();
})


//--------Chat bot handle--------------//

function CreateRespond(completeStatus, returnItem) {
    var newItem = {
        status: completeStatus,
        item: returnItem,
        message: "Heo nó mập thù lù mà nó stupid lắm mà :v"
    }
    return JSON.stringify(newItem);
}

//#endregion
