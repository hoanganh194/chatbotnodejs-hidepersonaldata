const MessengerPlatform = require('facebook-bot-messenger');
const DatabaseHandle = require("./Database")
const uuid = require('uuid')
class FacebookBot {
    constructor() {
        this.text = "";
        this.payload = "";
        this.quickMessage = "";

        this.userID = "";
        this.receiptID = "";
        this.token = "";

        this.bot = null;
        this.textBuilder = null;
        this.userData = null;
        this.product = null;
        this.promise = null;

        this.queue = null;
    }

    GetMessage(entries) {
        var ok = this.DecodeMessage(entries);
        if (ok == true) {
            console.log("Text: " + this.text + "|| Payload: " + this.payload + "||Quick:" + this.quickMessage + "||User: " + this.userID);
            (async() => {
                var result = await this.BuildTheBot();
                if (result) {
                    await this.GetUserData();
                    await this.DoActionBaseOnStatus();
                }
            })();
        }
    }

    async DoActionBaseOnStatus() {
        var commands = await DatabaseHandle.ReadCommandAction(this.receiptID, this.userData.Status);
        if (commands == null) {
            return;
        }
        for (var listAction of commands) {
            if (this.DoActionCheckCommandCondition(listAction)) {
                await this.HandleThisAction(listAction.Action);
            }
        }
        await DatabaseHandle.UpdateUserData(this.receiptID, this.userID, this.userData);
    }

    async HandleThisAction(listAction) {
        for (var action of listAction) {
            switch (action.Type) {
                case "Update":
                    await this.DoActionUpdateData(action);
                    break;
                case "Send":
                    await this.DoActionSendMessage(action);
                    break;
                case "Show":
                    await this.DoActionShowProduct(action);
                    break;
                case "Check":
                    await this.DoActionCheckAction(action);
                    break;
                case "Checkout":
                    await this.DoActionCheckOut(action);
                    break;
                case "Wait":
                    this.DoActionWaitForTimmer(action);
                    break;
            }
        }
    }
    DoActionWaitForTimmer(action) {

    }
    async DoActionCheckAction(action) {
        var valueCheck = this.GetValueFromMessageSubValue(action);
        var doAction = false;

        switch (action.Content) {
            case "IsNumber":
                doAction = this.GetNumberFromValue(valueCheck);
                break;
            case "IsPhone":
                doAction = this.GetNumberFromValue(valueCheck);
                break;
        }

        if (doAction) {
            if (action.Action != null) {
                await this.HandleThisAction(action.Action);
            }
        } else {
            if (action.ElseAction != null) {
                await this.HandleThisAction(action.ElseAction);
            }
        }
    }



    //#region Update data
    async DoActionUpdateData(action) {
        console.log("Update data");
        switch (action.Content) {
            case "Status":
                this.userData.Status = this.ReplaceValueFromText(action.SubValue);
                break;
            case "Phone":
                this.userData.Phone = this.ReplaceValueFromText(action.SubValue);
                break;
            case "Email":
                this.userData.Email = this.ReplaceValueFromText(action.SubValue);
                break;
            case "Address":
                this.userData.Address = this.ReplaceValueFromText(action.SubValue);
                break;
            case "Temp":
                await this.UpdateTempOrder(action);
                break;
            case "Order":
                this.UpdateOrderAndRemoveTemp(action);
                break;
        }
    }

    async UpdateTempOrder(action) {
        switch (action.Value) {
            case "@productID@":
                await this.UpdateTempProductFromMessage(action);
                break;
            case "@productQuantity@":
                await this.UpdateTempProductQuantityFromMessage(action);
                break;
            case "@productNameType@":
                await this.UpdateTempProductNameTypeFromMessage(action);
                break;
        }
    }

    async UpdateTempProductFromMessage(action) {
        var productID = this.GetValueFromMessageSubValue(action);
        this.product = await DatabaseHandle.GetProductDataWithID(this.receiptID, productID);
        if (this.product != null) {
            this.userData.Temp = {
                ProductID: this.product.ID,
                ProductName: this.product.Name,
                ProductNameTypeID: "",
                ProductNameType: "",
                ProductQuantity: 0
            }
        }
    }

    async UpdateTempProductNameTypeFromMessage(action) {
        var nameTypeID = this.GetValueFromMessageSubValue(action);
        this.product = await DatabaseHandle.GetProductDataWithID(this.receiptID, this.userData.Temp.ProductID);
        if (this.product != null) {
            for (var nameType of this.product.Price) {
                if (nameType.ID == nameTypeID) {
                    this.userData.Temp.ProductNameTypeID = nameType.ID;
                    this.userData.Temp.ProductNameType = nameType.NameType;
                    this.userData.Temp.ProductNameTypePrice = nameType.Price;
                    break;
                }
            }
        }
    }

    async UpdateTempProductQuantityFromMessage(action) {
        var quantity = this.GetValueFromMessageSubValue(action);
        if (this.userData.Temp != null) {
            this.product = await DatabaseHandle.GetProductDataWithID(this.receiptID, this.userData.Temp.ProductID);
            if (this.product != null) {
                this.userData.Temp.ProductQuantity = quantity;
            }
        }
    }

    UpdateOrderAndRemoveTemp(action) {
        if (this.userData.Temp != null) {
            this.userData.Order.push(this.userData.Temp);
            this.userData.Temp = null;
        }
    }

    GetValueFromMessageSubValue(action) {
        var value = "";
        switch (action.SubValue) {
            case "@payload@":
                value = this.payload;
                break;
            case "@text@":
                value = this.text;
                break;
            case "@quickmessage@":
                value = this.quickMessage;
                break;
            case "@address@":
                value = this.userData.Address;
                break;
            case "@phone@":
                value = this.userData.Phone;
                break;
            case "@email@":
                value = this.userData.Email;
                break;
        }
        return value;
    }

    //#endregion


    //#region  Send message
    async DoActionSendMessage(action) {
        console.log("Send message");
        switch (action.Content) {
            case "NormalMessage":
                await this.RespondNormalMessage(action);
                break;
            case "StructureMessage":
                await this.RespondStructureMessage(action);
                break;
            case "QuickMessage":
                await this.RespondQuickReplyMessage(action);
                break;
            case "TypeAction":
                await this.RespondSenderAction(action);
                break;

        }
    }
    async RespondSenderAction(action) {

    }
    async RespondNormalMessage(action) {
        await this.SendMessage(this.BuildNormalMessage(action.Value));
    }
    async RespondStructureMessage(action) {
        var listElement = [];
        listElement.push({
            Tittle: action.Value,
            Link: action.SubType,
            Text: action.SubValue,
            Image: action.DescValue,
            Options: action.Option,
        });
        await this.SendMessage(this.BuildListTempleMessage(listElement));
    }
    async RespondQuickReplyMessage(action) {
            await this.SendMessage(this.BuildQuickRepliesMessage(action.Value, action.Option));
        }
        //#endregion

    //#region Show product

    async DoActionShowProduct(action) {
        console.log("Show message");
        switch (action.Content) {
            case "FeatureProduct":
                await this.RespondShowProduct(action);
                break;
            case "SearchProduct":
                await this.RespondShowProduct(action);
                break;
            case "NameTypeOption":
                await this.RespondToShowNameTypeOption(action);
                break;
        }
    }
    async RespondShowProduct(action) {
        var searchQuery = this.GetValueFromMessageSubValue(action);
        var listShowProduct = await this.GetListShowingProduct(searchQuery);
        if (listShowProduct.length > 0) {
            var listElement = this.CreateArrayObjectShowProduct(listShowProduct);
            this.SendMessage(this.BuildListTempleMessage(listElement));
        } else {
            this.SendMessage(this.BuildNormalMessage("Không tìm thấy sản phẩm: " + this.text))
        }
    }
    async GetListShowingProduct(searchQuery) {
        var listShowProduct = [];
        var listProduct = await DatabaseHandle.GetListAllProduct(this.receiptID);

        if (searchQuery != "") {
            for (var product of listProduct) {
                if (product.Name.indexOf(searchQuery) < 0) {
                    continue;
                }
                listShowProduct.push(product);
            }
        } else {
            for (var product of listProduct) {
                if (!product.Feature) {
                    continue;
                }
                listShowProduct.push(product);
            }
        }
        return listShowProduct;
    }

    CreateArrayObjectShowProduct(listProduct) {
        var count = 0;
        var listElement = [];
     
        for (var product of listProduct) {
            var buttonsOption = [];
            buttonsOption.push({
                Text: "Mua",
                Payload: product.ID,
                ButtonType: "Payload"
            });
            console.log(product);
            listElement.push({
                Tittle: product.Name,
                Link: "",
                Text: product.Description,
                Image: product.Image,
                Options: buttonsOption

            })
            count++;
            if (count == 10) { break; }
        }
        return listElement;
    }
    CreateObjectShowNameTypeOption(product) {
        var count = 0;
        var listElement = [];
        var buttonsOption = [];
        for (var priceLine of product.Price) {
            buttonsOption.push({
                Text: priceLine.NameType,
                Payload: priceLine.ID,
                ButtonType: "Payload"
            });
            count++;
            if (count == 3) { break; }
        }
        listElement.push({
            Tittle: product.Name,
            Link: "",
            Text: product.Description,
            Image: product.Image,
            Options: buttonsOption

        })
        return listElement;
    }


    async RespondToShowNameTypeOption(action) {
            var productID = this.GetValueFromMessageSubValue(action);
            this.product = await DatabaseHandle.GetProductDataWithID(this.receiptID, productID);
            var listElement = this.CreateObjectShowNameTypeOption(this.product);
            this.SendMessage(this.BuildListTempleMessage(listElement));
        }
        //#endregion

    async DoActionCheckOut(action) {
        if (!this.userData.Order != null) {
            var user = this.bot.getProfile(this.userData.ID);
            var order = {
                userID: this.userID,
                userName: user,
                userPhone: this.userData.Phone,
                userAddress: this.userData.Address,
                userEmail: this.userData.Email,
                userOrder: this.userData.Order,
            }
            await DatabaseHandle.WriteUserOrder(this.receiptID, uuid.v1(), order);
        }
        this.ResetUserOrder();
    }
    ResetUserOrder() {
        this.userData.Status = 1;
        this.userData.Order = [];
        this.userData.Temp = null;
    }

    ReplaceValueFromText(text) {
        text = text.replace("@text@", this.text);
        text = text.replace("@payload@", this.payload);
        text = text.replace("@quickreply@", this.quickMessage);
        return text;
    }

    DoActionCheckCommandCondition(command) {
        if (!this.CheckSingleCommandCondition(this.text, command.TextCheck, command.TextCheckType)) {
            return false;
        }
        if (!this.CheckSingleCommandCondition(this.payload, command.PayloadCheck, command.PayloadCheckType)) {
            console.log(this.payload);
            console.log(command);
            return false;
        }

        if (!this.CheckSingleCommandCondition(this.quickMessage, command.QuickMessageCheck, command.QuickCheckType)) {
            return false;
        }
        return true;
    }
    CheckSingleCommandCondition(valueCheck, condition, checkType) {
        var result = false;
        if (checkType == "0") {
            result = true;
        } else if (checkType == "1") {
            if (valueCheck == condition) {
                result = true;
            } else {
                result = false;
            }
        } else if (checkType == "-1") {
            if (valueCheck != condition) {
                result = true;
            } else {
                result = false;
            }
        }
        //console.log("valueCheck: " + valueCheck + "|| condition: " + condition + "||checkType:" + checkType +"||result: " + result);
        return result;
    }


    async GetUserData() {
        var userData = await DatabaseHandle.GetUserData(this.receiptID, this.userID);
        if (userData == null) {
            this.userData = {
                ID: this.userID,
                Address: "",
                Phone: "",
                Email: "",
                Status: 1,
                Temp: "",
                Order: [],
                Command: ""
            }
        } else {
            this.userData = userData;
        }
    }

    DecodeMessage(entries) {
        var entry = entries[0];
        var messaging = entry.messaging;
        for (var message of messaging) {
            if (message.message != null) {
                if (message.message.is_echo == true) {
                    continue;
                }
            }
            if (message.delivery != null) {
                continue;
            }
            if (message.read != null) {
                continue;
            }
            this.GetMessageCoreFromMessage(message);
            return true;
        }

    }
    GetMessageCoreFromMessage(message) {
        this.text = "";
        this.payload = "";
        this.quickMessage = "";

        this.userID = "";
        this.token = "";
        this.receiptID = "";

        this.userID = message.sender.id;
        this.receiptID = message.recipient.id;
        console.log(message.message);
        if (message.message != null) {
            if (message.message.text) {
                this.text = message.message.text;
            }
            if (message.message.quick_reply) {
                this.quickMessage = message.message.quick_reply.payload;
            }
        }
        if (message.postback) {
            this.payload = message.postback.payload;
        }
    }

    async SendMessage(content) {
        this.bot.sendReadedAction(this.userID);
        await this.DelayForSecond(200);
        this.bot.sendTypingAction(this.userID);
        await this.DelayForSecond(1000);
        this.bot.sendClearTypingAction(this.userID);
        await this.DelayForSecond(200);
        this.bot.sendMessage(this.userID, content);
    }

    DelayForSecond(ms) {
        return new Promise((resolve) => { setTimeout(resolve, ms); });
    }


    BuildNormalMessage(text) {
        var builder = new MessengerPlatform.TextMessageBuilder(text);
        return builder;
    }

    BuildListTempleMessage(templateOptions) {
        console.log(templateOptions);
        var template = new MessengerPlatform.GenericTemplateBuilder();
        for (var templateOption of templateOptions) {
            var element = this.BuildTempleMessage(templateOption.Tittle, templateOption.Link, templateOption.Text, templateOption.Image, templateOption.Options);
            template.addElement(element);
        }
        var builder = new MessengerPlatform.AttachmentMessageBuilder(template);
        console.log(template);
        return builder;
    }

    BuildTempleMessage(tittle, link, text, image, options) {
        var element = new MessengerPlatform.GenericElementTemplateBuilder(tittle, link, image, text);
        for (var option of options) {
            switch (option.ButtonType) {
                case "Url":
                    element.addButton(new MessengerPlatform.URLButtonBuilder(option.Text, option.Payload));
                    break;
                case "Payload":
                    element.addButton(new MessengerPlatform.PostbackButtonBuilder(option.Text, option.Payload));
                    break;
            }
        }
        return element;
    }

    BuildQuickRepliesMessage(text, options) {
        var builder = new MessengerPlatform.QuickRepliesMessageBuilder(text);
        for (var option of options) {
            switch (option.ButtonType) {
                case "Text":
                    builder.addTextOption(option.Text, option.Payload, option.value);
                    break;
                case "Image":
                    builder.addImageOption(option.Text, option.Payload, option.value);
                    break;
            }
        }
        return builder;
    }

    async BuildTheBot() {
        var checkToken = await DatabaseHandle.GetToken(this.receiptID);
        //var checkToken = "";
        if (checkToken != null) {
            this.token = checkToken;
            console.log(this.token);
            this.bot = MessengerPlatform.create({
                pageID: this.receiptID,
                appID: "hide",
                appSecret: 'hide',
                validationToken: this.token,
                pageToken: this.token
            });
            return true;
        }
        return false;
    }

    GetNumberFromValue(str) {
        if (typeof str != "string") return false
        if (!isNaN(str) && !isNaN(parseFloat(str))) {
            return parseInt(str);
        }
    }

}
module.exports = FacebookBot