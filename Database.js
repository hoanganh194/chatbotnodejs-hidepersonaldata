const FS = require("fs")
const AwsDatabase = require("./AwsDatabase");
var CommandTable = "Command"
var UserTable = "UserData"
var TokenTable = "Token"
var ProductTable = "Product"
var OrderTable = "Order"

class Database {
    async ReadCommandAction(pageID, itemID) {
        var tableName = `${pageID}-${CommandTable}`;
        var item = await this.ReadItemDataWithID(tableName, itemID);
        return item;
    }

    async ReadListAllCommandActionID(pageID) {
        var tableName = `${pageID}-${CommandTable}`;
        var item = await this.ReadListAllItemID(tableName, "ListItem");
        return item;
    }

    async WriteCommandAction(pageID, itemID, item) {
        var tableName = `${pageID}-${CommandTable}`;
        await this.WriteItemToDatabase(tableName, itemID, item);
    }

    async DeleteCommandAction(pageID, itemID) {
        var tableName = `${pageID}-${CommandTable}`;
        await this.DeleteItem(tableName, itemID);
    }

    async GetUserData(pageID, userID) {
        var tableName = `${pageID}-${UserTable}`;
        var item = await this.ReadItemDataWithID(tableName, userID);
        return item;
    }

    async UpdateUserData(pageID, userID, userData) {
        var tableName = `${pageID}-${UserTable}`;
        await this.WriteItemToDatabase(tableName, userID, userData);
    }

    async GetListAllProduct(pageID) {
        var tableName = `${pageID}-${ProductTable}`;
        var returnData = await this.ReadAllItem(tableName);
        return returnData;
    }

    async GetListAllProductID(pageID) {
        var tableName = `${pageID}-${ProductTable}`;
        var returnData = await this.ReadListAllItemID(tableName);
        return returnData;
    }

    async GetProductDataWithID(pageID, productID) {
        var tableName = `${pageID}-${ProductTable}`;
        var item = await this.ReadItemDataWithID(tableName, productID);
        return item;
    }

    async GetUniqueProductID(pageID) {
        var tableName = `${pageID}-${ProductTable}`;
        var listID = await this.ReadListAllItemID(tableName);
        var randomID = 0;
        if (listID != null) {
            while (listID.includes(randomID.toString())) {
                randomID++;
            }
        }
        return randomID;
    }

    async DeleteProductWithID(pageID, productID) {
        var tableName = `${pageID}-${ProductTable}`;
        await this.DeleteItem(tableName, productID);
    }

    async UpdateProductDataWithID(pageID, productID, productData) {
        var tableName = `${pageID}-${ProductTable}`;
        await this.WriteItemToDatabase(tableName, productID, productData);
    }

    async WriteUserOrder(pageID, orderID, orderData) {
        var tableName = `${pageID}-${OrderTable}`;
        await this.WriteItemToDatabase(tableName, orderID, orderData);
    }

    async GetListUserOrderID(pageID) {
        var tableName = `${pageID}-${OrderTable}`;
        var returnData = await this.ReadListAllItemID(tableName);
        return returnData;
    }

    async GetListUserOrder(pageID) {
        var tableName = `${pageID}-${OrderTable}`;
        var returnData = await this.ReadAllItem(tableName);
        return returnData;
    }

    async GetUserOder(pageID, orderID) {
        var tableName = `${pageID}-${OrderTable}`;
        var returnData = await this.ReadItemDataWithID(tableName, orderID);
        return returnData;
    }

    async GetToken(pageID) {
        var tableName = `${pageID}-${TokenTable}`;
        var item = await this.ReadItemDataWithID(tableName, "1");
        return item;
    }

    async WriteUpdateToken(pageID, item) {
        var tableName = `${pageID}-${TokenTable}`;
        if (!await this.CheckTableExistFromDatabase(pageID)) {
            console.log("Call");
            await this.CreateAllTableForUser(pageID);
        }
        console.log("Table existed" + item);
        await this.WriteItemToDatabase(tableName, "1", item);

    }

    //#region Base read write function
    async ReadItemDataWithID(tableFullName, itemID) {
        var item = await AwsDatabase.FetchKey(tableFullName, itemID);
        if (item.Item) {
            var result = JSON.parse(item.Item.Value);
            return result;
        } else {
            return null;
        }
    }

    async ReadListAllItemID(tableFullName) {
        var item = await AwsDatabase.FetchKey(tableFullName, "ListItem");
        if (item.Item) {
            var result = JSON.parse(item.Item.Value);
            return result;
        } else {
            return null;
        }
    }

    async ReadAllItem(tableFullName) {
        var returnData = await AwsDatabase.GetListItem(tableFullName);
        var listItem = [];
        if (returnData.Items != null) {
            for (var item of returnData.Items) {
                listItem.push(JSON.parse(item.Value));
            }
        }
        return listItem;
    }

    async DeleteItem(tableFullName, itemID) {
        var listID = await this.ReadListAllItemID(tableFullName);
        if (listID.includes(itemID)) {
            await AwsDatabase.DeleteKey(tableFullName, itemID);
            var index = listID.indexOf(itemID.toString());
            if (index > -1) {
                listID.splice(index, 1);
            }
            listID = JSON.stringify(listID);
            await AwsDatabase.WriteKey(tableFullName, "ListItem", listID);
            return true;
        }
        return false;
    }


    async WriteItemToDatabase(tableFullName, itemID, item) {
        var listItemJson = await AwsDatabase.FetchKey(tableFullName, "ListItem");
        console.log(listItemJson);
        var listItem = [];
        if (listItemJson.Item) {
            listItem = JSON.parse(listItemJson.Item.Value);
        }
        if (listItem.find(x => x == itemID) == null) {
            listItem.push(itemID);
        }
        item = JSON.stringify(item, null, 4);
        listItem = JSON.stringify(listItem);
        await AwsDatabase.WriteKey(tableFullName, "ListItem", listItem);
        await AwsDatabase.WriteKey(tableFullName, itemID, item);
    }

    //#endregion


    async CreateAllTableForUser(pageID) {
        var tableName = `${pageID}-${CommandTable}`;
        await AwsDatabase.CreateTable(tableName);
        tableName = `${pageID}-${UserTable}`;
        await AwsDatabase.CreateTable(tableName);
        tableName = `${pageID}-${TokenTable}`;
        await AwsDatabase.CreateTable(tableName);
        tableName = `${pageID}-${ProductTable}`;
        await AwsDatabase.CreateTable(tableName);
        tableName = `${pageID}-${OrderTable}`;
        await AwsDatabase.CreateTable(tableName);
    }

    async CheckTableExistFromDatabase(pageID) {
        var tableName = `${pageID}-${TokenTable}`;
        var listTableName = await AwsDatabase.GetListTable();
        if (listTableName != null) {
            if (listTableName.TableNames.includes(tableName)) {
                console.log("Table existed");
                return true;
            }
        }
        return false;
    }

}

module.exports = new Database()