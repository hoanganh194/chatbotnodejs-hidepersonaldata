const { DynamoDB } = require("aws-sdk");
const AWS = require("aws-sdk")
const AWSConfig = {
    "region": "hide",
    "endpoint": "hide",
    "accessKeyId": "hide",
    "secretAccessKey": "hide"
}
AWS.config.update(AWSConfig);
let docClient = new AWS.DynamoDB.DocumentClient();
let database = new DynamoDB();
class AwsDatabase {
    async FetchKey(tableName, itemID) {
        itemID = String(itemID);
        var params = {
            TableName: tableName,
            Key: {
                "ID": itemID
            }
        };
        return await docClient.get(params).promise();
    }

    async WriteKey(tableName, itemID, itemValue) {
        var params = {
            TableName: tableName,
            Item: {
                ID: itemID,
                Value: itemValue
            }
        };
        return await docClient.put(params).promise();
    }
    async WriteKeyUniqueID(tableName, itemValue) {
        var params = {
            TableName: tableName,
            Item: {
                ID: context.awsRequestId,
                Value: itemValue
            }
        };
        return await docClient.put(params).promise();
    }
    async DeleteKey(tableName, itemID) {
        var params = {
            TableName: tableName,
            Key: {
                "ID": itemID
            }
        };
        return await docClient.delete(params).promise();
    }
    async CreateTable(tableName) {
        var params = {
            TableName: tableName,
            KeySchema: [{
                AttributeName: "ID",
                KeyType: "HASH"
            }],
            AttributeDefinitions: [{
                AttributeName: "ID",
                AttributeType: "S"
            }],
            ProvisionedThroughput: {
                ReadCapacityUnits: 1,
                WriteCapacityUnits: 1
            }
        };
        return await database.createTable(params).promise();
    }
    async GetListTable() {
        var params = {

        };
        return await database.listTables(params).promise();
    }
    async GetListItem(tableName) {
        var params = {
            TableName: tableName,
        };
        return await docClient.scan(params).promise();
    }
}

module.exports = new AwsDatabase()